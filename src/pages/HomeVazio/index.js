import { useRef, useState } from 'react';
import useLoginProvider from "../../hooks/useLoginProvider";
import Header from '../../components/Header';
import Card from '../../components/Card';
import Controls from '../../components/Controls';
import { musics } from '../../musics';
import './styles.css';

export default function App() {

  const { token, handleLogout } =
    useLoginProvider();

    const audioRef = useRef(null);

    const [iconBtn, setIconBtn] = useState('pause');
  
    const [musicsData, setMusicsData] = useState([...musics]);
    const [currentMusic, setCurrentMusic] = useState({
      id: 0,
      title: '',
      artist: ''
    });
  
    function setMusic(music) {
      setIconBtn('play');
  
      audioRef.current.src = music.url;
  
      setCurrentMusic(music);
    }
  
    function handleChangeMusic(option) {
      if (!currentMusic.id) {
        return;
      }
  
      const newMusicId = option === 'next'
        ? currentMusic.id + 1
        : currentMusic.id - 1;
  
      const otherMusic = musicsData.find((music) => music.id === newMusicId);
  
      if (!otherMusic) {
        return;
      }
  
      setMusic(otherMusic);
    }

  return (
    <section className='section-container'> 
     <Header
        handleLogout={handleLogout}
        token={token}
        titulo={"Home"}
      />
     <div className="container-music">
      <div>
        <h4>Bem vindo, aproveite para escutar uma musica.</h4>
      </div>

      <main>
        <div className='container-cards'>
          {musicsData.map((music) => (
            <div
              onClick={() => setMusic(music)}
              key={music.id}
            >
              <Card
                title={music.title}
                cover={music.cover}
              />
            </div>
          ))}
        </div>
      </main>
      
   </div>
   <Controls
        audioRef={audioRef}
        currentMusic={currentMusic}
        iconBtn={iconBtn}
        setIconBtn={setIconBtn}
        handleChangeMusic={handleChangeMusic}
      />

      <audio ref={audioRef} />
</section>
  );
}





