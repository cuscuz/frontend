![Logo](https://i.pinimg.com/originals/6a/92/44/6a9244c9cd387c4bf1e2822684f1211a.png)


<h1 align="center">
     Desafio 2 - Equipe 05
</h1>

![Badge em Desenvolvimento](http://img.shields.io/static/v1?label=STATUS&message=FINALIZADO&color=GREEN&style=for-the-badge)

## Índice 
* [Índice](#índice)
* [Descrição do Projeto](#descrição-do-projeto)
* [Pré-requisitos](#pré-requisitos)
* [Editar a aplicação ou rodar localmente](#editar-a-aplicação-ou-rodar-localmente)
* [Tecnologias utilizadas](#tecnologias-utilizadas)
* [Pessoas Desenvolvedoras](#pessoas-desenvolvedoras)


## Descrição do Projeto

O projeto consiste em três aplicações que conversam entre si, tendo como objetivo listar e cadastrar tanto os usuários, quanto os pedidos feitos pelos mesmos. Para isso, temos uma primeira tela de administrador, o qual será responsável por listar usuários, pedidos e também pelo cadastro de outros administradores.
A tela de administrador é a porta de acesso à aplicação, através da qual conseguimos percorrer as demais telas de usuários, pedidos e cadastro de novos administradores.

###  Pré-requisitos<a id="pre-requisitos"></a>

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
 [Git](https://git-scm.com/),
 [VSCode](https://code.visualstudio.com/).

## Editar a aplicação ou rodar localmente

````bash 
 # Clone este repositório
 git clone  https://gitlab.com/cuscuz/frontend
 # Acesse a pasta do projeto no terminal
 cd pasta
 
 # Instale as dependências
 $ yarn install ou
 $ npm i 
 
 # Execute a aplicação em modo de desenvolvimento
 $ npm start 
 $ yarn start
 
 # O servidor iniciará na porta:
 # Acesse http://localhost:
 ````

## Tecnologias utilizadas
 As seguintes linguagens/ferramentas foram usadas na construção do projeto:

## 🛠 Linguagem

 - [JavaScript](javaScript)

## 🛠 Tecnologia

 - [ReactJS](ReactJS)

 ## 🛠 Ferramentas
 - [Vscode](vscode)
 - [Jira](Jira)
 - [GitLab](GitLab)
  
## Pessoas desenvolvedoras

* ### Bianca Padilha <a href="https://www.linkedin.com/in/bianca-padilha-070772174/" target="_blank"><img src="https://user-images.githubusercontent.com/87882835/138565094-66ce9be2-2596-48ff-9a35-d03f166aa661.png" height="22px" width="24px" ></a> <a href="https://github.com/Padilha27" target="_blank"><img src="https://cdn-icons-png.flaticon.com/512/25/25231.png" height="24px" width="26px" > </a>
 
* ### Gabrielli Borges <a href="https://www.linkedin.com/in/gabrielli-borges-b02341207/" target="_blank"><img src="https://user-images.githubusercontent.com/87882835/138565094-66ce9be2-2596-48ff-9a35-d03f166aa661.png" height="22px" width="24px" > </a> <a href="https://github.com/Gabrielli5047" target="_blank"><img src="https://cdn-icons-png.flaticon.com/512/25/25231.png" height="24px" width="26px" > </a>
